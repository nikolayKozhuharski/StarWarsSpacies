import EventEmitter from "eventemitter3";
import Species from "./Species";
export default class StarWarsUniverse extends EventEmitter {
  constructor(maxSpacies = 10) {
    super()
    this.species = [];
    this._maxSpecies = maxSpacies;
  }
  static get events() {
    return {
      MAX_SPECIES_REACHED: "max_species_reached",
      SPECIES_CREATED: "species_created",
    };
  }
  get speciesCount() {
    return this.species.length;
  }
  _onSpeciesCreated(instance) {
    this.species.push(instance);
    this.emit(StarWarsUniverse.events.SPECIES_CREATED, this.speciesCount);
    if(this.speciesCount < this._maxSpecies) {
      this.createSpecies()
    }else {
      this.emit(StarWarsUniverse.events.MAX_SPECIES_REACHED)
    }

  }
  createSpecies() {
    let url = `https://swapi.booost.bg/api/species/${(this.speciesCount + 1)}`;
      const species = new Species();
      species.on(StarWarsUniverse.events.SPECIES_CREATED, () => this._onSpeciesCreated(species));
      species.init(url);
  }
}
